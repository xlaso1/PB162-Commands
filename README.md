# Cvicenie 02 - commands

Nastavenie gitu, aby nerobil rebase voci nasej branchi (inak by sme museli force-pushovat)
```
git config --global pull.rebase false     <---- fix na push do masteru!
```
Switch do mastera, potiahnutie nasich zmien po MR's, stiahnutie novej iteracie a potom push do masteru novej iteracie.
```
git checkout master
git fetch
git pull origin master

git pull munijava iteration-02 -X theirs

git push origin master
```
Switch do novej branchi, aby sme mohli pracovat na iteracii.
```
git checkout -b Submit-02
```

# Na konci cvicenia

Zobrazennie si suborov, kde nastala zmena a budu nejako ovplyvnene gitom
```
git status
```
Pridanie vsetkych suborov z directory
```
git add -A
```
Pridanie specifickeho suboru z directory
```
git add /somethere/in/the/project/Hello.java
```
commit a push do nasej vytvorenej branche.
```
git commit -m "## YOUR MESSAGE ##"
git push origin Submit-02
```

# Praca z domu
pull nasej rozrobenej iteracie.
```
git pull origin Submit-01
git fetch
```


# In case of git errors
if the pull is giving you some unknown error and you dont have much time to take care of it,
you can try the following command for the pull.
```
git pull --rebase munijava iteration-0X -X theirs
```
